import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class App {
    int findID;


    private JButton stepButton;
    private JPanel panel1;
    private JButton UP1Button1;
//    private JButton DOWN1Button1;
    private JButton UP2Button2;
    private JButton DOWN2Button3;
    private JButton UP3Button3;
    private JButton UP4Button4;
    private JButton DOWN3Button2;
    private JButton DOWN4Button4;
//    private JButton UP5Button5;
    private JButton DOWN5Button5;
    private JButton viewLiftStatusButton;
    private JButton a1Button;
    private JButton a2Button;
    private JButton a3Button;
    private JButton a4Button;
    private JButton a5Button;
    private JTextArea textArea1;
    private JButton DOWN1Button1;
    private JButton UP5Button5;

    public void appendText(String text)
    {
        textArea1.append(text);
    }

    public void setText(String text)
    {
        textArea1.setText(text);
    }
    public JPanel getPanel1() {
        return panel1;
    }

    public App() {
        final int NUMBER_OF_LIFTS = 4;
        LiftSystem elSystem = new LiftSystem();


        for (int i = 0; i < NUMBER_OF_LIFTS; i++)
        {
            elSystem.addLift(new Lift(i));
        }


        stepButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elSystem.step();
            }
        });

        UP1Button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(1, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(1);
            }
        });

        UP2Button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(2, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(2);
//                System.out.println("\nZnaleziono windę " + elSystem.mapLift.get(findID) );
            }
        });

        UP3Button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(3, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(3);
            }
        });

        UP4Button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(4, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(4);
            }
        });

//
        DOWN2Button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(2, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(2);
            }
        });

        DOWN3Button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(3, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(3);
            }
        });

        DOWN4Button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(4, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(4);
            }
        });

        DOWN5Button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findID = elSystem.pickup(5, 1);
                elSystem.mapLift.get(findID).setM_currentFloor(5);
            }
        });

        a1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elSystem.update(findID, elSystem.mapLift.get(findID).getM_currentFloor(), 1);
            }
        });

        a2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elSystem.update(findID, elSystem.mapLift.get(findID).getM_currentFloor(), 2);
            }
        });

        a3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elSystem.update(findID, elSystem.mapLift.get(findID).getM_currentFloor(), 3);
            }
        });

        a4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elSystem.update(findID, elSystem.mapLift.get(findID).getM_currentFloor(), 4);
            }
        });

        a5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elSystem.update(findID, elSystem.mapLift.get(findID).getM_currentFloor(), 5);
            }
        });

        viewLiftStatusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setText("");
                for(int i = 0; i < NUMBER_OF_LIFTS; i++)
                {
                    String dest_fl;
                    if (elSystem.mapLift.get(i).getM_destinationFloor() == Lift.NOFLOOR)
                        dest_fl = "none";
                    else
                        dest_fl = Integer.toString(elSystem.mapLift.get(i).getM_destinationFloor());
                    appendText( "id: " +
                        (elSystem.mapLift.get(i).getM_liftID()) + " current floor: " +
                        (elSystem.mapLift.get(i).getM_currentFloor()) + " destination floor: " +
                        dest_fl + "\n"
                    );
                }


            }
        });

    }
}

