import javax.lang.model.type.NullType;

public class Lift {
    public static final int NOFLOOR = -100;
    int m_liftID;
    int m_currentFloor = 0;
    int m_destinationFloor = NOFLOOR;



    static int counter = 0;



    Lift(int lift)
    {
        m_liftID = lift;
    }

    Lift()
    {
        counter++;
        m_liftID = counter;
    }

    @Override
    public String toString()
    {
        return "id: " + m_liftID + "; current floor: " + m_currentFloor + "; destination floor: " + m_destinationFloor;
    }

    public int getM_currentFloor() {
        return m_currentFloor;
    }

    public int getM_destinationFloor() {
        return m_destinationFloor;
    }

    public int getM_liftID() {
        return m_liftID;
    }

    public void setM_currentFloor(int m_currentFloor) {
        this.m_currentFloor = m_currentFloor;
    }

    public void setM_destinationFloor(int m_destinationFloor) {
        this.m_destinationFloor = m_destinationFloor;
    }
}
