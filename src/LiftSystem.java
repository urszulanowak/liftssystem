import java.util.HashMap;
import java.util.Map;
import java.lang.Math;

public class LiftSystem {
    public final int maxFloor = 5;
    Map<Integer, Lift> mapLift = new HashMap<>();

    public Map status(){
        return mapLift;
    }

    public int pickup(int current, int direction)
    {
        int i = maxFloor;
        Lift result = null;
        for (var entry : mapLift.entrySet()) //if any lift free
        {
            if((entry.getValue().getM_currentFloor() == entry.getValue().getM_destinationFloor() || entry.getValue().getM_destinationFloor() == Lift.NOFLOOR) &&  Math.abs(current - entry.getValue().getM_currentFloor()) < i)
            {
                i = Math.abs(current - entry.getValue().getM_currentFloor());
                result = entry.getValue();
            }
        }
        if(result == null)
        {
            for (var entry : mapLift.entrySet())
            {
                if(direction > 0 && entry.getValue().getM_currentFloor() < current && entry.getValue().m_destinationFloor > current)
                {
                    result = entry.getValue();
                }
            }
        }
        if(result == null)
        {
            for (var entry : mapLift.entrySet())
            {
                if(direction < 0 && entry.getValue().getM_currentFloor() > current && entry.getValue().m_destinationFloor < current)
                {
                    result = entry.getValue();
                }
            }
        }
        if(result == null)
        {
            this.step();
            for (var entry : mapLift.entrySet()) //if any lift free
            {
                if((entry.getValue().getM_currentFloor() == entry.getValue().getM_destinationFloor() || entry.getValue().getM_destinationFloor() == Lift.NOFLOOR) &&  Math.abs(current - entry.getValue().getM_currentFloor()) < i)
                {
                    i = Math.abs(current - entry.getValue().getM_currentFloor());
                    result = entry.getValue();
                }
            }
        }
        return result.getM_liftID();
    }

    public void update(int id, int current, int destination)
    {
        if(mapLift.get(id).getM_currentFloor() == mapLift.get(id).m_destinationFloor || mapLift.get(id).m_destinationFloor == Lift.NOFLOOR)
        {
            mapLift.get(id).setM_currentFloor(current);
            mapLift.get(id).setM_destinationFloor(destination);
            return;
        }

    }

    public void step()
    {
        for ( var entry : mapLift.entrySet())
        {
            if(entry.getValue().getM_destinationFloor() != Lift.NOFLOOR)
                entry.getValue().setM_currentFloor(entry.getValue().getM_destinationFloor());
        }
    }

    public void addLift(Lift l)
    {
        mapLift.put(l.getM_liftID(), l);
    }

    public void printLifts()
    {
        for (var entry : mapLift.entrySet())
        {
            System.out.println("Lift id: " + entry.getKey() + "-\t-" + entry.getValue());
        }
    }


}
