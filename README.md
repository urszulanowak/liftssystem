## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [How to run](#run)
* [GUI Guide](#gui)
* [Future Plans](#plans)

## General info
This project contains elevator control system. It consists of an algorithm for elevator management and GUI for demonstration of its abilities. 

## Technologies
Project is created with:
* Java 11
* Swing
* Oracle Open JDK 19
* ItelliJ IDEA
* Git

## How to run
This project has not been deployed yet. You need to open it in IntellIJ.

## GUI guide
The buttons were made according to email sent documentation. At first the user should click the desired direction from the current floor. All floors are visible at once in the GUI. At second the destination floor should be selected. The "View lift status" can be used anytime to see the elevators' status. The "Step" button performs one step in lift simulation.

The elevators count has been limited to 4 because of better readability. It can be changed any time in App.java by modifying NUMBER_OF_LIFTS final field. The number of floors can also be modified by maxFloor in LiftSystem.java. It could be made in the runtime, but it is sufficient for debug purposes. I am sorry for the inconvenience of recompiling the app by your own. 

## Future plans 
* Adding simulation with real time
* GUI development 
* Optimizing algorithms


